import React, {useState, useEffect, useContext} from 'react';

/*react router dom*/
import { useHistory, Redirect } from 'react-router-dom';

/*react-bootstrap components*/
import {Container, Form, Button, Row, Col} from 'react-bootstrap';

/*context*/
import UserContext from './../UserContext';

/*sweetalert*/
import Swal from 'sweetalert2';



export default function Register(){
	const [name, setName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [verifyPassword, setVerifyPassword] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);

	const {user} = useContext(UserContext)

	let history = useHistory();

	useEffect( () => {
		if(email !== '' && password !== '' && verifyPassword !== '' && password === verifyPassword){
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
	}, [email, password, verifyPassword]);


	function register(e){
		e.preventDefault();

		// alert('Registration Successful, you may now log in');
		fetch('https://artixen-service.onrender.com/api/users/checkEmail', {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then( result => result.json())
		.then( result => {
			// console.log(result)	//boolean

			if(result === true){
				// alert("User already exist")
				Swal.fire({
					title: 'Duplicate email found',
					icon: 'error',
					text: 'Please choose another email'
				})
			} else {
				//what to do when user/email still not existing?

				fetch('https://artixen-service.onrender.com/api/users/register', {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						name: name,
						email: email,
						password: password
					})
				})
				.then( result => result.json())
				.then( result => {
					//console.log(result)	//boolean

					if(result === true){

						Swal.fire({
							title: "Registration Successful",
							icon: "success",
							text: "Proceed to login"
						})

						history.push('/login');

					} else {
						Swal.fire({
							title: 'Something went wrong',
							icon: 'error',
							text: 'Please try again'
						})
					}
				})

			}
		})
		setName('');
		setEmail('');
		setPassword('');
		setVerifyPassword('');
	}

	return(
		(user.id !== null) ?

			<Redirect to="/" />

		:
			<Container fluid className="mb-5">
				<Row>
					<Col md={4}>

					</Col>
					<Col xs={12} md={4}>
						<h3 className="text-center mt-4">Register</h3>
						<Form className="mt-4 mx-3" onSubmit={(e)=> register(e)}>
							<Form.Group className="mb-3" controlId="formName">
								<Form.Label>Full Name</Form.Label>
								<Form.Control type="text" className="rounded-pill" value={name}
								onChange={(e)=> setName(e.target.value) }/>
							</Form.Group>

							<Form.Group className="mb-3" controlId="formBasicEmail">
								<Form.Label>Email address</Form.Label>
								<Form.Control type="email" className="rounded-pill" value={email}
								onChange={(e)=> setEmail(e.target.value) }/>
							</Form.Group>

							<Form.Group className="mb-3" controlId="formBasicPassword">
								<Form.Label>Password</Form.Label>
								<Form.Control type="password" className="rounded-pill" value={password}
								onChange={(e)=> setPassword(e.target.value) }/>
							</Form.Group>

							<Form.Group className="mb-3" controlId="formVerifyPassword">
								<Form.Label>Verify Password</Form.Label>
								<Form.Control type="password" className="rounded-pill" value={verifyPassword}
								onChange={(e)=> setVerifyPassword(e.target.value)}/>
							</Form.Group>

							<Form.Group className="text-center d-grid">
							<Button className="btn btn-dark rounded-pill" type="submit" disabled={isDisabled}>Submit</Button>
							</Form.Group>


						</Form>
					</Col>
				</Row>
				
			</Container>
	)
}