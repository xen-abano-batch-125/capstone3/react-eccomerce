import React, {useEffect} from 'react';
import {Container, Table} from 'react-bootstrap'

export default function MyOrders(){

/*	const [id, setId] = useState('');
	const [name, setName] = useState('');
	const [desc, setDesc] = useState('');
	const [price, setPrice] = useState(0);*/

/*	const [orders, setOrders] = useState([]);*/

	

	useEffect( () => {
			const orderData = () => {
				let token = localStorage.getItem('token')

				fetch('https://artixen-service.onrender.com/api/users/myOrders',{
					method: "GET",
					headers: {
						"Authorization": `Bearer ${token}`
					}
				})
				.then(result => result.json())
				.then(result => {
		/*			console.log(result.orders)*/

				})
			}
		orderData()
	}, [])
 


	return(
		<Container>
			<div>
				<h2 className="text-center">My Orders</h2>
			</div>
			<Table>
				<thead>
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
					</tr>
				</thead>
				<tbody>

				</tbody>
			</Table>
		</Container>
	)
}

/*					<tr key={orders._id}>
						<td>{orders.name}</td>
						<td>{orders.desc}</td>
						<td>{orders.price}</td>
					</tr>*/