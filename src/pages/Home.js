import React from 'react'

/*react-bootstrap component*/
import Container from 'react-bootstrap/Container'

/*components*/
import Banner from './../components/Banner';
import Feature from './../components/Feature';

export default function Home(){

	return(
		<Container fluid>
			<Banner/>
			<Feature/>
		</Container>
	)
}