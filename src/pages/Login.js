import React, {useState, useEffect, useContext} from 'react';
import { Redirect } from 'react-router-dom'

/*Context*/
import UserContext from './../UserContext';

/*react-bootstrap components*/
import {Container, Form, Button, Row, Col} from 'react-bootstrap';


export default function Login(){
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);

	/*destructure context object*/
	const {user, setUser} = useContext(UserContext);

	useEffect( () => {
		if(email !== '' && password !== ''){
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
	}, [email, password]);

	function login(e){
		e.preventDefault();

		// alert('Login Successful');
		fetch('https://artixen-service.onrender.com/api/users/login', {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(result => result.json())
		.then(result => {
			console.log(result) //{access: token}

			if(typeof result.access !== "undefined"){
				//what should we do with the access token?
				localStorage.setItem('token', result.access)
				userDetails(result.access)
			}
		})

		const userDetails = (token) => {
			fetch('https://artixen-service.onrender.com/api/users/details',{
				method: "GET",
				headers: {
					"Authorization": `Bearer ${token}`
				}
			})
			.then(result => result.json())
			.then(result => {
				console.log(result) //whole user object or document

				setUser({
					id: result._id,
					isAdmin: result.isAdmin
				});
			})
		}

		setEmail('');
		setPassword('');
	}

	return(
		(user.id !== null) ? 

			<Redirect to="/" />

		: 
		<Container fluid >
		  <Row>
		  	<Col md={4}>
		  	</Col>
		    <Col xs={12} md={4}>
		    	<h3 className="text-center mt-5">Login</h3>
		    	<Form className="mt-4 mx-3" onSubmit={ (e) => login(e) } >
		    		<Form.Group className="mb-3" controlId="formBasicEmail">
		    			<Form.Label>Email address</Form.Label>
		    			<Form.Control type="email" className="rounded-pill" value={email}
		    			onChange={(e)=> setEmail(e.target.value) }/>
		    		</Form.Group>

		    		<Form.Group className="mb-3" controlId="formBasicPassword">
		    			<Form.Label>Password</Form.Label>
		    			<Form.Control type="password" className="rounded-pill" value={password}
		    			onChange={(e)=> setPassword(e.target.value) }/>
		    		</Form.Group>

		    		<Form.Group className="d-grid text-center">
		    		<Button className="btn btn-dark rounded-pill" type="submit" disabled={isDisabled}>Submit</Button>
		    		</Form.Group>
		    	</Form>
		    </Col>

		  </Row>
		</Container>
	)
}