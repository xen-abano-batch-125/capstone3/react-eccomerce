import React, { useState, useEffect} from 'react'; //, useContext 
import { Container, Form, Button, Row, Col } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
/*import UserContext from '../UserContext';*/

import Swal from 'sweetalert2'

export default function AddProduct(){

/*	const { user } = useContext(UserContext);*/
	const history = useHistory();

	const [name, setName] = useState('');
	const [desc, setDesc] = useState('');
	const [price, setPrice] = useState(0);
	const [isActive, setIsActive] = useState(true);

	let token = localStorage.getItem('token')

	useEffect(()=>{

		if(name !== '' && desc !== '' && price !== 0){
			setIsActive(true);
		}else{
			setIsActive(false);
		}

	}, [name, desc, price]);


	function addProduct(e){

		e.preventDefault();

		fetch('https://artixen-service.onrender.com/api/products/addProduct', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				desc: desc,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data === true){

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Product successfully added!"
				})

				history.push('/products');

			} else {

				Swal.fire({
					title: "Failed",
					icon: "error",
					text: "Please try again"
				})

			}
		})

		setName('');
		setDesc('');
		setPrice(0);

	};


	return(
		<Container className="my-5">
			<Row>
				<Col xs={12} md={4}></Col>
				<Col xs={12} md={4}>
				<h3 className="text-center mb-4">Add Product</h3>
				<Form onSubmit={ e => addProduct(e)}>
					<Form.Group>
						<Form.Label>Product Name:</Form.Label>
						<Form.Control
							className="rounded-pill mb-3"
							type="text"
							value={name}
							onChange={(e) => setName(e.target.value)}
						/>
					</Form.Group>

					<Form.Group>
						<Form.Label >Description:</Form.Label>
						<Form.Control
							className="rounded-pill mb-3"
							type="text"
							value={desc}
							onChange={(e) => setDesc(e.target.value)}
						/>
					</Form.Group>

					<Form.Group>
						<Form.Label>Price:</Form.Label>
						<Form.Control
							className="rounded-pill mb-3"
							type="number"
							value={price}
							onChange={(e) => setPrice(e.target.value)}
						/>
					</Form.Group>

					<Form.Group className="d-grid mt-3">
						{ 
							(isActive === true) ? 
								<Button type="submit" className="rounded-pill btn-dark">Submit</Button>
							:
								<Button type="submit" className="rounded-pill btn-dark" disabled>Submit</Button>
						}
					</Form.Group>
					
				</Form>
				</Col>
			</Row>
		</Container>
		)
}