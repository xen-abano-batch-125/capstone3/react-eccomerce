import React, {useContext, useEffect, useState, Fragment} from 'react';

import UserContext from './../UserContext';

import {Link, useParams, useHistory} from 'react-router-dom';

import {Container, Card, Button, Row, Col} from 'react-bootstrap';

import Swal from 'sweetalert2';


export default function SpecificProduct(){

	const [name, setName] = useState('');
	const [desc, setDesc] = useState('');
	const [price, setPrice] = useState(0)

	const { user } = useContext(UserContext);

	const { productId } = useParams();

	let token = localStorage.getItem('token')

	let history = useHistory();

	useEffect( () => {

		fetch(`https://artixen-service.onrender.com/api/products/${productId}`,
			{
				method: "GET",
				headers: {
					"Authorization": `Bearer ${token}`
				}
			}
		)
		.then(result => result.json())
		.then(result => {
			console.log(result)

			setName(result.name);
			setDesc(result.desc);
			setPrice(result.price);
		})

	}, [token, productId])

	const checkout = () => {
		fetch('https://artixen-service.onrender.com/api/users/checkout', 
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					productId: productId
				})
			}
		)
		.then(result => result.json())
		.then(result => {
			console.log(result)

			if(result === true){

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Order Placed." 
				})

				history.push('/products');
			} else {
				Swal.fire({
					title: "Failed",
					icon: "error",
					text: "Please try again" 
				})
			}
		})
	}

	return(
		<Container fluid>
			<Row>
				<Col md={4}></Col>
				<Col md={4}>
					<Card className="justify-content-center gap-2 mb-3 mt-3">
						<Card.Body>
							<div className="d-grid">
							{
								(user.id !== null) ?
										<Fragment>
											<Card.Img variant="top" src="https://images.unsplash.com/photo-1593721627612-4c8376834dcc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=80" />
											<Card.Title className="text-center mt-3">{name}</Card.Title>
											<p className="text-center">Php {price}</p>
											<p className="text-center">{desc}</p>
											<Button className="btn-dark rounded-pill" onClick={ () => checkout() }>Checkout</Button>
										</Fragment>
									:
										<Link className="btn btn-dark rounded-pill d-md-flex justify-content-center" to="/login">Login</Link>
							}
							</div>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}
