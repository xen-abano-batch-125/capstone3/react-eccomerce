import React, {useState, useEffect} from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import'./App.css';
/*Context*/
import UserContext from './UserContext';

import AppNavbar from './components/AppNavbar';
import Register from './pages/Register';
import Login from './pages/Login';
import Home from './pages/Home'
import Products from './pages/Products';
import AddProduct from './pages/AddProduct';
import SpecificProduct from './pages/SpecificProduct';
import MyOrders from './pages/MyOrders';
/*import AllOrders from './pages/AllOrders';*/

export default function App(){
	const [user, setUser] = useState(
		{
			id: null,
			isAdmin: null
		}
	);

	const unsetUser = () => {
		localStorage.clear();
		setUser({
			id: null,
			isAdmin: null
		})
	}

	useEffect( () => {
		let token = localStorage.getItem('token');
		fetch('https://artixen-service.onrender.com/api/users/details', {
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
/*			console.log(result) //object/ document of a user*/

			if(typeof result._id !== "undefined"){
				setUser({
					id: result._id,
					isAdmin: result.isAdmin
				})
			} else {
				setUser({
					id: null,
					isAdmin: null
				})
			}
		})
	}, [])

	return( 

	<UserContext.Provider value={{user, setUser, unsetUser}}> 
		<BrowserRouter>
			<AppNavbar/>
			<Switch>
				<Route exact path="/" component={Home} />
				<Route exact path="/login" component={Login} />
				<Route exact path="/register" component={Register} />
				<Route exact path="/products" component={Products} />
				<Route exact path="/addProduct" component={AddProduct} />
				<Route exact path="/products/:productId" component={SpecificProduct} />
				<Route exact path="/myOrders" component={MyOrders} />
			</Switch>
		</BrowserRouter>
	</UserContext.Provider>
	)
}