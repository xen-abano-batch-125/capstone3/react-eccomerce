import React from 'react';


/*react-bootstrap components*/
import {
	Container,
	Row,
	Col,
	Jumbotron
} from 'react-bootstrap';

export default function Banner(){

	return(
		<Container fluid className="mb-4">
			<Row>
				<Col>
					<Jumbotron fluid className="px-5 py-5">
					  <h1>ARTIXEN</h1>
					  <h4>where art meets expression.</h4>
					</Jumbotron>
				</Col>
			</Row>
		</Container>
	)
}
