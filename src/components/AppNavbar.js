import React, {Fragment, useContext} from 'react';
import { Link, NavLink, useHistory } from 'react-router-dom';
import UserContext from './../UserContext';
import {Navbar, Nav} from 'react-bootstrap';

export default function AppNavbar(){
	const {user, unsetUser} = useContext(UserContext);
	let history = useHistory();
	const logout = () => {
	    unsetUser();
	    history.push('/login');
	  }
	let leftNav = (user.id !== null) ? 
	        (user.isAdmin === true) ?
	          <Fragment>
	            <Nav.Link as={NavLink} to="/addProduct">Add Product</Nav.Link>
{/*	            <Nav.Link as={NavLink} to="/allOrders">Orders</Nav.Link>*/}
	            <Nav.Link onClick={logout}>Logout</Nav.Link>
	          </Fragment>
	        :
	          <Fragment>
{/*	          	<Nav.Link as={NavLink} to="/myOrders">Orders</Nav.Link>*/}
	            <Nav.Link onClick={logout}>Logout</Nav.Link>
	          </Fragment>
	    :
	      (
	        <Fragment>
	            <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
	            <Nav.Link as={NavLink} to="/login" className="btn btn-dark btn-sm rounded-pill text-white">Login</Nav.Link>
	          </Fragment>

	      )

	  return (
	    <Navbar bg="transparent" expand="lg">
	      <Navbar.Brand as={Link} to="/" className="fw-bold mx-4"><i className="bi bi-x-diamond-fill"></i>   Artixen</Navbar.Brand>
	      <Navbar.Toggle aria-controls="basic-navbar-nav" />
	      <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end mx-4">
	        <Nav>
	          <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
	        </Nav>
	        <Nav>
	          	{leftNav}
	        </Nav>
	      </Navbar.Collapse>
	    </Navbar>
	  )
}