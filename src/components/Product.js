import React from 'react';
import PropTypes from 'prop-types';

/*react-bootstrap components*/
import {Card, Container, Row, Col} from 'react-bootstrap';

import {Link} from 'react-router-dom';

export default function Product({productProp}){
/*	console.log(productProp)*/

	const {name, desc, price, _id} = productProp

	return(
		<Container fluid>
			<Row>
				<Col xs={12} md={3}></Col>
				<Col md={6}>
					<Card className="justify-content-center gap-2 mb-4">
						<Card.Img variant="top" src="https://images.unsplash.com/photo-1593721627612-4c8376834dcc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=80" />
						<Card.Body>
							<Card.Title className="text-center">{name}</Card.Title>
							<p className="text-center">Php {price}</p>
							<p className="text-center">{desc}</p>
					    	<Link className="d-grid btn btn-dark rounded-pill" to={`/products/${_id}`}>
					    		Details
					    	</Link>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}


Product.propTypes = {
	product: PropTypes.shape({
		name: PropTypes.string.isRequired,
		desc: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}