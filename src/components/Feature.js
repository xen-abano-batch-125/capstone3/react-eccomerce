import React, {useContext} from 'react'; //useState,
import UserContext from './../UserContext';
import {Link} from 'react-router-dom';
import {Container, CardGroup, Card} from 'react-bootstrap';



export default function Feature(){
	const { user } = useContext(UserContext);
	return(

		<Container className="mb-5">
		<CardGroup>
		  <Card>
		    <Card.Img variant="top" src="https://images.unsplash.com/photo-1588014328208-de6c5973a014?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=873&q=80"/>
		    <Card.Body>
		      <Card.Title>Artist Paint Brushes</Card.Title>
		      <Card.Text>
		        The paintbrush will be your magic wand for weaving colors across the canvas. Artist paint brushes become beloved tools the more you use them!
		      </Card.Text>
		    </Card.Body>
		    <Card.Footer>
		    	<div className="d-grid">
					{
						(user.id !== null) ?
								<Link className="btn btn-dark rounded-pill d-md-flex justify-content-center" to="/products">Read more</Link>
							:
								<Link className="btn btn-dark rounded-pill d-md-flex justify-content-center" to="/login">Read more</Link>
					}
				</div>
		    </Card.Footer>
		  </Card>
		  <Card>
		    <Card.Img variant="top" src="https://images.unsplash.com/photo-1595446472613-b18ef68e21bc?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80"/>
		    <Card.Body>
		      <Card.Title>Get to know your paintbrush</Card.Title>
		      <Card.Text>
		        A paint brush is made of four main parts: bristles, ferrule, crimp, and handle. Easy enough! So now that you know the lingo, let's find out what each brush is meant for!
		      </Card.Text>
		    </Card.Body>
		    <Card.Footer>
		    	<div className="d-grid">
					{
						(user.id !== null) ?
								<Link className="btn btn-dark rounded-pill d-md-flex justify-content-center" to="/products">Read more</Link>
							:
								<Link className="btn btn-dark rounded-pill d-md-flex justify-content-center" to="/login">Read more</Link>
					}
				</div>
		    </Card.Footer>
		  </Card>
		  <Card>
		    <Card.Img variant="top" src="https://images.unsplash.com/photo-1600693437693-e3eb10df2677?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=871&q=80"/>
		    <Card.Body>
		      <Card.Title>How to Choose the Right Paintbrush</Card.Title>
		      <Card.Text>
		        To help navigate the wide variety of paintbrushes on the market, we’ve outlined a few of the most common types, and what they can be used for.
		      </Card.Text>
		    </Card.Body>
		    <Card.Footer>
		    	<div className="d-grid">
					{
						(user.id !== null) ?
								<Link className="btn btn-dark rounded-pill d-md-flex justify-content-center" to="/products">Read more</Link>
							:
								<Link className="btn btn-dark rounded-pill d-md-flex justify-content-center" to="/login">Read more</Link>
					}
				</div>
		    </Card.Footer>
		  </Card>
		</CardGroup>

		</Container>

	)
}