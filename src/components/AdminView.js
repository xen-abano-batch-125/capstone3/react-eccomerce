import React, {useState, useEffect, Fragment} from 'react'
import {Container, Table, Button, Modal, Form} from 'react-bootstrap'

import Swal from 'sweetalert2';

export default function AdminView(props){
/*	console.log(props)*/

	const { productData, fetchData } = props;

	const [productId, setProductId] = useState('');
	const [products, setProducts] = useState([]);
	const [name, setName] = useState('');
	const [desc, setDesc] = useState('');
	const [price, setPrice] = useState(0);

	const [showEdit, setShowEdit] = useState(false);
	const [showAdd, setShowAdd] = useState(false);

	let token = localStorage.getItem('token');

	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false);

	const closeEdit = () => {

		setShowEdit(false);
		setName("")
		setDesc("")
		setPrice(0)
	}

	useEffect( () => {
		const productsArr = productData.map( (product) => {
			const openEdit = (productId) => {
				fetch(`https://artixen-service.onrender.com/api/products/${productId}`,{
					method: "GET",
					headers: {
						"Authorization": `Bearer ${token}`
					}
				})
				.then(result => result.json())
				.then(result => {
					console.log(result)

					setProductId(result._id);
					setName(result.name);
					setDesc(result.desc);
					setPrice(result.price)
				})

				setShowEdit(true);
			}
			
			/*update product*/
			const archiveToggle = (productId, isActive) => {

				fetch(`https://artixen-service.onrender.com/api/products/${productId}/archive`, {
					method: "PUT",
					headers: {
						"Content-Type": "application/json",
						"Authorization": `Bearer ${token}`
					},
					body: JSON.stringify({
						isActive: isActive
					})
				})
				.then(result => result.json())
				.then(result => {
					console.log(result)

					fetchData();
					if(result === true){
						Swal.fire({
							title: "Success",
							icon: "success",
							"text": "Product availability status updated."
						})
					} else {
						fetchData();
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							"text": "Please try again"
						})
					}
				})
			}

			const unarchiveToggle = (productId, isActive) => {
				fetch(`https://artixen-service.onrender.com/api/products/${productId}/unarchive`, {
					method: "PUT",
					headers: {
						"Content-Type": "application/json",
						"Authorization": `Bearer ${token}`
					},
					body: JSON.stringify({
						isActive: isActive
					})
				})
				.then(result => result.json())
				.then(result => {
					console.log(result)

					fetchData();
					if(result === true){
						Swal.fire({
							title: "Success",
							icon: "success",
							"text": "Product availability status updated."
						})
					} else {
						fetchData();
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							"text": "Please try again"
						})
					}
				})
			}

			const deleteToggle = (productId) => {
				fetch(`https://artixen-service.onrender.com/api/products/${productId}/delete`, {
					method: "DELETE",
					headers: {
						"Authorization": `Bearer ${token}`
					}
				})
				.then(result => result.json())
				.then(result => {
					console.log(result)

					fetchData();
					if(result === true){
						Swal.fire({
							title: "Success",
							icon: "success",
							"text": "Product successfully deleted!"
						})
					} else {
						fetchData();
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							"text": "Please try again"
						})
					}
				})
			}

/*			console.log(product)*/
			return(
				<tr key={product._id}>
					<td>{product.name}</td>
					<td>{product.desc}</td>
					<td>{product.price}</td>
					<td>
						{
							(product.isActive === true) ?
								<span>Available</span>
							:
								<span>Unavailable</span>
						}
					</td>
					<td>
						<Fragment>
							<span size="sm" className="me-1"
							onClick={ ()=> openEdit(product._id) }>
								<i className="bi bi-pencil-square"></i>
							</span>
							<span size="sm" className="me-1"
							onClick={ () => deleteToggle(product._id)}>
								<i className="bi bi-trash-fill"></i>
							</span>
						</Fragment>

						{
							(product.isActive === true) ?
								<span size="sm"
								onClick={()=> archiveToggle(product._id, product.isActive)}>
									<i className="bi bi-archive-fill"></i>
								</span>
							:
								
								<span size="sm"
								onClick={ () => unarchiveToggle(product._id, product.isActive)}>
									<i className="bi bi-archive-fill"></i>
								</span>
								
						}

					</td>
				</tr>
			)
		})
		setProducts(productsArr)

	}, [productData, fetchData, token])

	/*edit product function*/
	const editProduct = (e, productId) => {

		e.preventDefault()

		fetch(`https://artixen-service.onrender.com/api/products/${productId}/edit`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				desc: desc,
				price: price
			})
		})
		.then(result => result.json())
		.then(result => {
			console.log(result) //updated product document

			fetchData()

			if(typeof result !== "undefined"){
				// alert("success")

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Product successfully updated!"
				})

				closeEdit();
			} else {

				fetchData()

				Swal.fire({
					title: "Failed",
					icon: "error",
					text: "Something went wrong!"
				})
			}
		})
	}





	const addProduct = (e) => {
		e.preventDefault()
		fetch('https://artixen-service.onrender.com/api/products/addProduct', {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				desc: desc,
				price: price
			})
		})
		.then(result => result.json())
		.then(result => {
			console.log(result)

			if(result === true){
				fetchData()

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Product successfully added"
				})

				setName("")
				setDesc("")
				setPrice(0)

				closeAdd();

			} else {
				fetchData();

				Swal.fire({
					title: "Failed",
					icon: "error",
					text: "Something went wrong"
				})
			}
		})
	}

	return(
		<Container>
			<div>
				<h2 className="text-center">Admin Dashboard</h2>
				<div className="d-flex justify-content-center gap-2">
					<div className="mb-4">
						<div onClick={openAdd}><i className="bi bi-plus-circle-fill"></i></div>
					</div>
				</div>
			</div>
			<Table>
				<thead>
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					{products}
				</tbody>
			</Table>
		{/*Edit Product Modal*/}
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={ (e) => editProduct(e, productId) }>
					<Modal.Header>
						<Modal.Title>Edit Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control
								type="text"
								value={name}
								onChange={ (e)=> setName(e.target.value)}
							/>
						</Form.Group>
						<Form.Group controlId="productDesc">
							<Form.Label>Description</Form.Label>
							<Form.Control
								type="text"
								value={desc}
								onChange={ (e)=> setDesc(e.target.value)}
							/>
						</Form.Group>
						<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control
								type="number"
								value={price}
								onChange={ (e)=> setPrice(e.target.value)}
							/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button className="rounded-pill btn-dark" onClick={closeEdit}>Close</Button>
						<Button className="rounded-pill btn-dark" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		{/*Add Product Modal*/}
		<Modal show={showAdd} onHide={closeAdd}>
			<Form onSubmit={ (e) => addProduct(e) }>
				<Modal.Header>Add Product</Modal.Header>
				<Modal.Body>
					<Form.Group productId="productName">
						<Form.Label>Name</Form.Label>
						<Form.Control 
							type="text"
							value={name}
							onChange={(e)=> setName(e.target.value)}
						/>
					</Form.Group>
					<Form.Group productId="productDesc">
						<Form.Label>Description</Form.Label>
						<Form.Control
							type="text"
							value={desc}
							onChange={(e)=> setDesc(e.target.value)}
						/>
					</Form.Group>
					<Form.Group productId="productPrice">
						<Form.Label>Price</Form.Label>
						<Form.Control 
							type="number"
							value={price}
							onChange={(e)=> setPrice(e.target.value)}
						/>
					</Form.Group>
				</Modal.Body>
				<Modal.Footer>
					<Button className="rounded-pill btn-dark" onClick={closeAdd}>Close</Button>
					<Button className="rounded-pill btn-dark" type="submit">Submit</Button>
				</Modal.Footer>
			</Form>
		</Modal>
		</Container>
	)
}